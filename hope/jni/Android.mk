ROOT_LOCAL_PATH := $(call my-dir)

WEBKIT:=libmywebkit.so
ifneq (, $(strip $(wildcard $(ROOT_LOCAL_PATH)/$(WEBKIT))))
    LOCAL_PATH := $(ROOT_LOCAL_PATH)
    include $(CLEAR_VARS)
    LOCAL_MODULE    := webkit4droid
    LOCAL_SRC_FILES := $(WEBKIT)
    include $(PREBUILT_SHARED_LIBRARY)
endif

MAME:=libmame.so
ifneq (, $(strip $(wildcard $(ROOT_LOCAL_PATH)/$(MAME))))
    LOCAL_PATH := $(ROOT_LOCAL_PATH)
    include $(CLEAR_VARS)
    LOCAL_MODULE    := mame4droid
    LOCAL_SRC_FILES := $(MAME)
    include $(PREBUILT_SHARED_LIBRARY)
endif
