/*
 * Copyright (C) 2014 armite@126.com
 *
 */

package com.armite.WebKit;

import java.nio.ByteBuffer;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Paint.Style;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Toast;
import java.io.File;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.view.KeyEvent;
import android.content.res.AssetManager;
import android.view.SurfaceHolder.Callback;
import android.view.MotionEvent;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDevice;
import android.content.Context;
import java.util.HashMap;
import java.util.Iterator;

public class MameView extends SurfaceView implements Callback {
    final private static String TAG = "MameView";

    private HopeView mHopeView = null;
    private Thread mNativeMainT = null;
    private Paint mPaint = null;
    private final Bitmap.Config mFormat = Bitmap.Config.ARGB_8888;
    //private final Bitmap.Config mFormat = Bitmap.Config.RGB_565;
    private AudioTrack audioTrack = null;
    private Rect mRectSrc = null;
    private Rect mRectDst = null;

    public MameView(Hope hope, HopeView view, final String libPath, int width, int height) {
        super(hope);
        mHopeView = view;
        getHolder().addCallback(this);
        loadMameLib(libPath);

        setFocusable(true);
        mPaint = new Paint(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        mBitmap = Bitmap.createBitmap(width, height, mFormat);
        mRectSrc = new Rect(0, 0, width, height);

        UsbManager manager = (UsbManager) hope.getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        while (deviceIterator.hasNext())
        {
            UsbDevice device = deviceIterator.next();
            Log.i(TAG, "usb name:" + device.getDeviceName());
        }
    }

    public void play(final String rompath, final String name) {
        if (mNativeMainT != null)
            return;
        mNativeMainT = new Thread(new Runnable() {
            public void run() {
                mHopeView.focuseMameView();
                Log.i(TAG, "start the game : " + name);
                runGame(mHopeView.getAssetManager(), rompath, name);
                Log.i(TAG, "exit the game : " + name);
                mHopeView.focuseWebView();
                mNativeMainT = null;
            }
        }, "MameMain");

        mNativeMainT.setPriority(Thread.MAX_PRIORITY);
        mNativeMainT.start();
    }

    public void pause() { sendKeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_P); }
    public void resume() { sendKeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_R); }
    public void stop() {
        sendKeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_E);
        try{
            if (mNativeMainT != null)
                mNativeMainT.join();
        } catch (InterruptedException e) {
            Log.i(TAG, "wait native thread exit error!!!");
            e.printStackTrace();
        }
    }

    private boolean onKeyEvent(int KeyCode, KeyEvent event) {
        if (KeyCode == KeyEvent.KEYCODE_BACK)
            return false;
        switch (KeyCode) {
            case KeyEvent.KEYCODE_DPAD_UP:      KeyCode = KeyEvent.KEYCODE_W; break;
            case KeyEvent.KEYCODE_DPAD_DOWN:    KeyCode = KeyEvent.KEYCODE_S; break;
            case KeyEvent.KEYCODE_DPAD_LEFT:    KeyCode = KeyEvent.KEYCODE_A; break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:   KeyCode = KeyEvent.KEYCODE_D; break;
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_BUTTON_A:     KeyCode = KeyEvent.KEYCODE_J; break;
            case KeyEvent.KEYCODE_9:
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_BUTTON_B:     KeyCode = KeyEvent.KEYCODE_K; break;
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_BUTTON_START: KeyCode = KeyEvent.KEYCODE_1; break;
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_MENU:
            case KeyEvent.KEYCODE_BUTTON_R1:    KeyCode = KeyEvent.KEYCODE_5; break;
        }
        sendKeyEvent(event.getAction(), KeyCode);
        return true;
    }

    @Override
    public boolean onKeyDown(int KeyCode, KeyEvent event) {
        Log.i(TAG, event.getRepeatCount() + " onKeyDown:"+KeyCode);
        return onKeyEvent(KeyCode, event);
    }

    @Override
    public boolean onKeyUp(int KeyCode, KeyEvent event) {
        Log.i(TAG, event.getRepeatCount() + " onKeyUp:"+KeyCode);
        return onKeyEvent(KeyCode, event);
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        Log.i(TAG, event.getRawX() + "   "+event.getRawY());
        return super.onGenericMotionEvent(event);
    }

    @Override
    public boolean onKeyLongPress(int KeyCode, KeyEvent event) {
        Log.i(TAG, event.getRepeatCount() + " onKeyLongPress:"+KeyCode);
        return onKeyEvent(KeyCode, event);
    }

    @Override
    public boolean onKeyMultiple(int KeyCode, int count, KeyEvent event) {
        Log.i(TAG, event.getRepeatCount() + " onKeyLongPress "+ count +" :"+KeyCode);
        return onKeyEvent(KeyCode, event);
    }

    private SoundThread soundT = null;
    private int audioOpen(int sampleFreq, boolean stereo)
    {
        int channelConfig = stereo ? AudioFormat.CHANNEL_OUT_STEREO : AudioFormat.CHANNEL_OUT_MONO;
        int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
        int bufferSize = AudioTrack.getMinBufferSize(sampleFreq, channelConfig, audioFormat);
        Log.i(TAG, "audioOpen bufferSize :" + bufferSize);

        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleFreq,
                channelConfig,
                audioFormat,
                bufferSize * 2,
                AudioTrack.MODE_STREAM);
        Log.i(TAG, "audioOpen channelConfig :" + audioTrack.getChannelCount());
        audioTrack.play();
        if (soundT == null)
            soundT = new SoundThread(audioTrack, bufferSize);
        return bufferSize;
    }

    private void audioClose()
    {
        if(audioTrack != null) {
            audioTrack.stop();
            audioTrack.release();
            audioTrack = null;
        }
    }

    private void audioPlay(byte[] b, int sz)
    {
        if(soundT != null) {
            soundT.writeSample(b, sz);
        } else
            audioTrack.write(b, 0, sz);
    }

    private Bitmap mBitmap = null;
    private Bitmap getBitmap(int width, int height) {
        if (width > mBitmap.getWidth() || height > mBitmap.getHeight()) {
            Log.i(TAG, "Bitmap recreate:" + width + " x " + height);
            mBitmap = Bitmap.createBitmap(width, height, mFormat);
            mRectSrc = new Rect(0, 0, width, height);
        }
        return mBitmap;
    }

    private SurfaceHolder mSurfaceHolder = null;
    private void updateBitmap() {
        if (mSurfaceHolder == null)
            return;
        Canvas cv = null;
        try {
            cv = mSurfaceHolder.lockCanvas();
            cv.drawBitmap(mBitmap, mRectSrc, mRectDst, mPaint);
        } catch (Exception e) {
            Log.i(TAG, "updateWindow error:" + e.getMessage());
            e.printStackTrace();
        } finally {
            if (cv != null)
                mSurfaceHolder.unlockCanvasAndPost(cv);
        }
    }

    public void startupShowing() { mHopeView.showHopeView(); }

    public void surfaceCreated(SurfaceHolder holder) { mSurfaceHolder = holder; }
    public void surfaceDestroyed(SurfaceHolder holder) { mSurfaceHolder = null; }
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) { mRectDst = new Rect(0, 0, width, height); }

    //native
    private native void runGame(AssetManager am, String path, String name);
    synchronized public native void sendKeyEvent(int type, int keycode);

    /* load our native library */
    private void loadMameLib(final String libpath) {
        Log.i("mame_load", "load library :" + libpath);
        try {
            System.load(libpath);
        } catch (Error e) {
            Log.i("mame_load", "load library error:" + libpath);
            e.printStackTrace();
            System.loadLibrary("mame");
        }
    }

    class SoundThread implements Runnable {
        private Thread t = null;
        private AudioTrack audioTrack;
        private byte[] buff;
        private int buff_size;

        public SoundThread(AudioTrack audio, int size) {
            audioTrack = audio;
            buff = new byte[size];
            buff_size = 0;
            t = new Thread(this, "sound-Thread");
            t.start();
        }

        @Override
        public void run() {
            while (true) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                if (audioTrack != null) {
                    audioTrack.write(buff, 0, buff_size);
                }
            }
        }

        synchronized public void writeSample(byte[] b, int size) {
            System.arraycopy(b, 0, buff, 0, size);
            buff_size = size;
            this.notify();
        }
    }
}

