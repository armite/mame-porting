/*
 * Copyright (C) 2014 armite@126.com
 *
 */
package com.armite.WebKit;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.content.pm.ApplicationInfo;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.opengl.GLSurfaceView;
import android.view.KeyEvent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.widget.FrameLayout;
import android.widget.VideoView;
import android.widget.MediaController;
import android.net.Uri;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.view.Surface;
import android.graphics.BitmapFactory;
import android.os.SystemClock;
import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import android.content.res.AssetManager;

class HopeView extends FrameLayout {
    private final String TAG = "HopeView";
    private Hope mHope = null;
    private static final int BROWSERWIDTH = 1280;
    private static final int BROWSERHEIGHT = 720;
    private WebView mWebView = null;
    private static final int MAMEWIDTH = 384;
    private static final int MAMEHEIGHT = 224;
    private MameView mMameView = null;

    public HopeView(Hope hope) {
        super(hope);
        mHope = hope;

        String libpath = releaseLib("libmame.so");
        if (libpath != null) {
            mMameView = new MameView(mHope, this, libpath, MAMEWIDTH, MAMEHEIGHT);
            addView(mMameView);
            focuseMameView();
        }

        libpath = releaseLib("libmywebkit.so");
        if (libpath != null) {
            mWebView = new WebView(mHope, this, libpath, BROWSERWIDTH, BROWSERHEIGHT);
            addView(mWebView);
            focuseWebView();
        }
    }

    final private int BUFFER_SIZE = 1024 * 1024;
    private String releaseLib(String libName) {
        String libPath = mHope.getApplicationInfo().nativeLibraryDir + File.separator + libName;
        File file = new File(libPath);
        if (file.exists())
            return libPath;

        libPath = mHope.getApplicationInfo().dataDir + File.separator + libName;
        file = new File(libPath);
        if (file.exists() && file.canRead())
            return libPath;

        try {
            List<String> lists = Arrays.asList(mHope.getAssets().list(""));
            if (!lists.contains(libName))
                return null;
            InputStream fis = mHope.getAssets().open(libName);
            BufferedInputStream src = new BufferedInputStream(fis);
            FileOutputStream fos = new FileOutputStream(file);
            BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER_SIZE);
            byte data[] = new byte[BUFFER_SIZE];
            int count;
            while ((count = src.read(data, 0, BUFFER_SIZE)) != -1)
                dest.write(data, 0, count);
            dest.flush();
            dest.close();
            src.close();
        } catch (Exception e) {
            Log.i(TAG, "release lib " + libName + " error!");
            e.printStackTrace();
            libPath = null;
        }
        return libPath;
    }

    public void focuseWebView() {
        mHope.runOnUiThread(new Runnable() {
            public void run() {
                if (mMameView != null)
                    mMameView.clearFocus();
                if (mWebView != null)
                    mWebView.requestFocus();
            }
        });
    }

    public void focuseMameView() {
        mHope.runOnUiThread(new Runnable() {
            public void run() {
                if (mWebView != null)
                    mWebView.clearFocus();
                if (mMameView != null)
                    mMameView.requestFocus();
            }
        });
    }

    public WebView getWebView() {
        return mWebView;
    }

    public MameView getMameView() {
        return mMameView;
    }

    public AssetManager getAssetManager() {
        return mHope.getAssets();
    }

    public void showHopeView() {
        mHope.showHopeView();
    }
}

