/*
 * Copyright (C) 2014 armite@126.com
 *
 */
package com.armite.WebKit;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.content.Intent;
import android.content.Context;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.content.pm.ApplicationInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.KeyEvent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.graphics.PixelFormat;
import android.widget.VideoView;
import android.widget.MediaController;
import android.net.Uri;
import android.graphics.Point;
import android.view.Display;
import android.content.res.AssetManager;
import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.zip.ZipEntry;
import java.util.Enumeration;
import java.util.zip.ZipFile;
import java.util.zip.ZipException;
import android.view.inputmethod.InputMethodManager;
import android.os.ResultReceiver;
import android.graphics.Color;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

public class Hope extends Activity {
    private final String TAG = "Hope Activity";

    private HopeView mHopeView = null;
    private BootView mBootView = null;
    private Handler mHandler = null;
    private final int SHOW_HOPE = 0x01;
    private final int HIDE_HOPE = SHOW_HOPE + 1;

    //private final String mWebViewHome = "http://www.baidu.com";
    private String mWebViewHome = "file:///sdcard/test.html";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER);
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_DISPLAY);

        mBootView = new BootView(this);
        setContentView(mBootView);

        mHopeView = new HopeView(this);
        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case SHOW_HOPE:
                        setContentView(mHopeView);
                        break;
                    case HIDE_HOPE:
                        setContentView(mBootView);
                        break;
                }
            }
        };
    }

    private final int bootShowTime = 2 * 1000;
    public void showHopeView() {
        mHandler.sendEmptyMessageDelayed(SHOW_HOPE, bootShowTime);
        //mHandler.sendEmptyMessageDelayed(HIDE_HOPE, bootShowTime + 1 * 1000);
    }

    private long lastBackKeyTime = 0;
    @Override
    public boolean onKeyDown(int KeyCode, KeyEvent event) {
        Log.i(TAG, event.getRepeatCount() + " onKeyDown:"+KeyCode);
        if (KeyCode == KeyEvent.KEYCODE_BACK) {
            long currentTime = System.currentTimeMillis();
            if (lastBackKeyTime == 0 || currentTime > lastBackKeyTime + 3000) {
                lastBackKeyTime = currentTime;
                Toast.makeText(this, "在按一次返回键退出", Toast.LENGTH_SHORT).show();
            } else
                finish();
            return true;
        }
        return  super.onKeyDown(KeyCode, event);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "****** activity lifecycle onStart pid:"  + android.os.Process.myPid());
        if (mHopeView.getMameView() != null)
            mHopeView.getMameView().play(null, getResources().getString(R.string.app_name));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "****** activity lifecycle onResume");
        if (mHopeView.getMameView() != null)
            mHopeView.getMameView().resume();
    }

    @Override
    protected void onPause() {
        if (mHopeView.getMameView() != null)
            mHopeView.getMameView().pause();
        super.onPause();
        Log.i(TAG, "****** activity lifecycle onPause");
    }

    @Override
    protected void onStop() {
        if (mHopeView.getMameView() != null)
            mHopeView.getMameView().stop();
        super.onStop();
        Log.i(TAG, "****** activity lifecycle onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "****** activity lifecycle onDestroy");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i(TAG, "****** onLowMemory");
    }
}



