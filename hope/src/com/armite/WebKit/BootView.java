/*
 * Copyright (C) 2014 armite@126.com
 *
 */

package com.armite.WebKit;

import java.nio.ByteBuffer;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Paint.Style;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Toast;
import java.io.File;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.view.KeyEvent;
import android.content.res.AssetManager;
import android.view.SurfaceHolder.Callback;
import java.io.InputStream;
import java.util.ArrayList;
import java.net.URL;
import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.util.Arrays;
import org.json.JSONObject;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;

public class BootView extends SurfaceView implements Callback {
    final private static String TAG = "BootView";

    private Hope mHope = null;
    final private static String BootName = "Boot.png";
    private Bitmap mBootBitmap = null;

    public BootView(Hope hope) {
        super(hope);
        mHope = hope;
        getHolder().addCallback(this);
        Log.i(TAG, "BootView init");
    }

    HttpGetThread mHttpThread = null;
    public void surfaceCreated(SurfaceHolder holder) { }
    public void surfaceDestroyed(SurfaceHolder holder) { }
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i(TAG, "BootView surfaceChanged");
        String bootPath = mHope.getApplicationInfo().dataDir + File.separator + BootName;
        File bootFile = new File(bootPath);
        if (bootFile.exists() && bootFile.canRead()) {
            mBootBitmap = BitmapFactory.decodeFile(bootPath);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date lastTime = new Date(bootFile.lastModified() + 23*24*60*60*1000);
            Date curDate = new Date();
            Log.i(TAG, "Bitmap expire time:" + sdf.format(lastTime));
            if (curDate.after(lastTime))
                bootFile.delete();
        } else {
            try {
                InputStream fis = mHope.getAssets().open(BootName);
                mBootBitmap = BitmapFactory.decodeStream(fis);
            } catch (Exception e) {
                Log.i(TAG, "get boot bitmap error");
                e.printStackTrace();
            }
        }

        try {
            Canvas cv = holder.lockCanvas();
            Rect srcRect = new Rect(0, 0, mBootBitmap.getWidth(), mBootBitmap.getHeight());
            Rect dstRect = new Rect(0, 0, width, height);
            Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG | Paint.ANTI_ALIAS_FLAG);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
            cv.drawBitmap(mBootBitmap, srcRect, dstRect, paint);
            holder.unlockCanvasAndPost(cv);
        } catch (Exception e) {
            Log.i(TAG, "show boot bitmap error");
            e.printStackTrace();
        }

        if (mHttpThread == null)
            mHttpThread = new HttpGetThread();
    }

    class HttpGetThread implements Runnable {
        private Thread mThread;
        public HttpGetThread() {
            mThread = new Thread(this, "http-Thread");
            mThread.start();
        }

        private ArrayList<byte[]> httpGet(String uri) {
            ArrayList<byte[]> retContent = new ArrayList<byte[]>();
            HttpURLConnection urlConnection = null;
            InputStream in = null;
            try {
                URL url = new URL(uri);
                urlConnection = (HttpURLConnection)url.openConnection();
                in = new BufferedInputStream(urlConnection.getInputStream());
                int length = 0;
                byte data[] = new byte[8192];
                while (length >= 0) {
                    length = in.read(data, 0, 8192);
                    if (length > 0)
                        retContent.add(Arrays.copyOf(data, length));
                }
                in.close();
            } catch (Exception e) {
                Log.i(TAG, "http get error, uri:" + uri);
                e.printStackTrace();
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
            return retContent;
        }

        //private final String appengineURL = "http://10.0.5.67:8080";
        //private final String appengineURL = "https://armite-mame.appspot.com";
        private final String appengineURL = "https://raw.githubusercontent.com/armite/armite-mame/master";
        private final String jsonURL = appengineURL + "/data.json?";
        private String imageURL = null;
        private int imageSize = 0;
        private String showTime = null;
        @Override
        public void run() {
            Log.i(TAG, "http get json start");
            ArrayList<byte[]> retContent = httpGet(jsonURL + mHope.getResources().getString(R.string.app_name));
            if (retContent.isEmpty()) {
                Log.i(TAG, "http get json nothing");
                return;
            }

            String json = new String(retContent.get(0));
            Log.i(TAG, "http get json: " + json);
            try {
                JSONObject jsonObject = new JSONObject(json);
                imageURL = jsonObject.getString("imageURL");
                imageSize = jsonObject.getInt("imageSize");
                showTime = jsonObject.getString("showTime");
                Log.i(TAG, "imageURL: " + imageURL + ",imageSize:" + imageSize + ",showTime:" + showTime);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date showDate = sdf.parse(showTime);
                Date curDate = new Date();
                Log.i(TAG, "current date: " + sdf.format(curDate));
                if (curDate.after(showDate)) {
                    Log.i(TAG, "show image date expire!");
                    String bootPath = mHope.getApplicationInfo().dataDir + File.separator + BootName;
                    File bootFile = new File(bootPath);
                    if (bootFile.exists() && bootFile.canRead()) {
                        Log.i(TAG, "delete boot png in data!");
                        bootFile.delete();
                    }
                    return;
                }
            } catch (Exception e) {
                Log.i(TAG, "http parse json error!!!");
                e.printStackTrace();
                return;
            }

            String bootPath = mHope.getApplicationInfo().dataDir + File.separator + BootName;
            File bootFile = new File(bootPath);
            if (bootFile.exists()) {
                Log.i(TAG, "exists file length: " + bootFile.length());
                if (bootFile.length() == imageSize) {
                    Log.i(TAG, "http get image use current file!");
                    return;
                }
                bootFile.delete();
            }

            Log.i(TAG, "http get image start");
            if (!imageURL.startsWith("http://"))
                imageURL = appengineURL + imageURL;
            retContent = httpGet(imageURL);
            if (retContent.isEmpty()) {
                Log.i(TAG, "http get image nothing!!!");
                return;
            }
            int contSize = 0;
            for (int i = 0; i < retContent.size(); i++)
                contSize += retContent.get(i).length;
            Log.i(TAG, "http get image size: " + contSize);
            if (contSize != imageSize) {
                Log.i(TAG, "http get image size error!!!");
                return;
            }

            try {
                if (!bootFile.createNewFile()) {
                    Log.i(TAG, "http get image createNewFile error!!!");
                    return;
                }
                OutputStream out = new BufferedOutputStream(new FileOutputStream(bootFile));
                for (int i = 0; i < retContent.size(); i++)
                    out.write(retContent.get(i));
                out.flush();
                out.close();
            } catch (Exception e) {
                Log.i(TAG, "write file error");
                e.printStackTrace();
                return;
            }
            Log.i(TAG, "http get image write NewFile ok!");
        }
    }
}

