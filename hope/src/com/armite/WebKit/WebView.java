/*
 * Copyright (C) 2014 armite@126.com
 *
 */
package com.armite.WebKit;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.content.pm.ApplicationInfo;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Surface;
import android.view.KeyEvent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.os.Handler;
import android.os.Message;
import android.os.Looper;
import java.io.File;
import android.os.HandlerThread;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;

public class WebView extends TextureView implements TextureView.SurfaceTextureListener {
    /* implementend by libmywebkit.so */
    public native void BrowserConfig(String key, String value);
    private native void InitBrowser(Bitmap bitmap);
    private native void setSurface(Surface sur, int width, int height);
    private native void loadURL(String url);
    private native void ExitBrowser();
    private native void fireWebViewTimer();
    private native void onExpose(boolean isRefresh);
    private native void onKeyEvent(int key, int unicode);
    private native void onMouseEvent(String type, int x, int y);

    /* load our native library */
    private void loadLib(final String libpath) {
        Log.i("webkit_load", "load library :" + libpath);
        try {
            System.load(libpath);
        } catch (Error e) {
            Log.i("webkit_load", "load library error:" + libpath);
            e.printStackTrace();
            System.loadLibrary("mywebkit");
        }
    }

    /* this is only one instance */
    private static final String TAG = "WebView";
    private int mViewWidth = 1280;
    private int mViewHeight = 720;
    private HopeView mHopeView = null;
    public WebView(Hope hope, HopeView view, final String libpath, int width, int height) {
        super(hope);
        mHopeView = view;
        loadLib(libpath);
        mViewWidth = width;
        mViewHeight = height;
        //BrowserConfig("GEFO_MSAA_GL_COMPOSITOR", "on");

        initBrowser();
        setSurfaceTextureListener(this);
        setFocusable(true);
    }

    public boolean loadWeb(final String url) {
        return mHandler.post(new Runnable(){
            public void run(){
                loadURL(url);
            }
        });
    }

    public void exitBrowser() {
        mHandler.post(new Runnable(){
            public void run(){
                ExitBrowser();
            }
        });
    }

    @Override
    public boolean onKeyDown(int KeyCode, KeyEvent event) {
        Log.i(TAG, event.getRepeatCount() + " onKeyDown:"+KeyCode);
        int vKey = 0, unicode = 0;
        switch(KeyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:   vKey = 0x25; break;
            case KeyEvent.KEYCODE_DPAD_UP:     vKey = 0x26; break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:  vKey = 0x27; break;
            case KeyEvent.KEYCODE_DPAD_DOWN:   vKey = 0x28; break;
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER: vKey = 0x0D; break;
            case KeyEvent.KEYCODE_DEL:
            case KeyEvent.KEYCODE_BACK:        vKey = 0x1B; break;
        }

        if (vKey > 0 || unicode > 0) {
            Message msg = mHandler.obtainMessage(WEBVIEW_KEYEVENT, vKey, unicode);
            mHandler.sendMessage(msg);
            return true;
        }
        return super.onKeyDown(KeyCode, event);
    }

    private int m_actionDownX = 0;
    private int m_actionDownY = 0;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        String type = null;
        int x = (int) event.getX() * mViewWidth / mDstRect.width();
        int y = (int) event.getY() * mViewHeight / mDstRect.height();
        Log.i(TAG, "onTouchEvent:"+x+","+y);
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            m_actionDownX = x;
            m_actionDownY = y;
            type = "mouse-down";
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            type = "mouse-up";
            if (x > m_actionDownX + 9 || x < m_actionDownX - 9
                    || y > m_actionDownY + 9 || y < m_actionDownY - 9) {
                    x = m_actionDownX;
                    y = m_actionDownY;
            }
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (x > m_actionDownX + 9 || x < m_actionDownX - 9
                    || y > m_actionDownY + 9 || y < m_actionDownY - 9)
                type = "mouse-move";
        }
        if (type != null) {
            Message msg = mHandler.obtainMessage(WEBVIEW_MOUSEEVENT, x, y, type);
            mHandler.sendMessage(msg);
            return true;
        }
        return super.onTouchEvent(event);
    }

    public void sendExposeEvent() {
        Message msg = mHandler.obtainMessage(WEBVIEW_EXPOSE);
        mHandler.sendMessage(msg);
    }

    public void setTimer(long delay) {
        Message msg = mHandler.obtainMessage(WEBVIEW_TIMER);
        mHandler.sendMessageDelayed(msg, delay);
    }

    private final int WEBVIEW_KEYEVENT      = 0x02;
    private final int WEBVIEW_MOUSEEVENT    = 0x04;
    private final int WEBVIEW_TIMER         = 0x06;
    private final int WEBVIEW_EXPOSE        = 0x08;
    private HandlerThread mHandlerThread = null;
    private Handler mHandler = null;
    private Bitmap mBitmap = null;
    private void initBrowser() {
        mHandlerThread = new HandlerThread(TAG, Thread.MAX_PRIORITY);
        mHandlerThread.start();

        mHandler = new Handler(mHandlerThread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case WEBVIEW_KEYEVENT:
                        onKeyEvent(msg.arg1, msg.arg2);
                        break;
                    case WEBVIEW_MOUSEEVENT:
                        onMouseEvent(msg.obj.toString(), msg.arg1, msg.arg2);
                        break;
                    case WEBVIEW_EXPOSE:
                        removeMessages(WEBVIEW_EXPOSE);
                        onExpose(true);
                        break;
                    case WEBVIEW_TIMER:
                    default:
                        removeMessages(WEBVIEW_TIMER);
                        fireWebViewTimer();
                }
            }
        };

        mHandler.post(new Runnable() {
            public void run(){
                mBitmap = Bitmap.createBitmap(mViewWidth, mViewHeight, Bitmap.Config.ARGB_8888);
                InitBrowser(mBitmap);
                setTimer(1);
            }
        });
    }

    private final boolean isGLES2 = false;
    private final Rect mSrcRect = new Rect();
    private final Rect mDstRect = new Rect();
    private Paint mPaint = null;
    private void updateWindow(int x, int y, int w, int h) {
        if (!isGLES2) {
            Log.i(TAG, "updateWindow invoked:"+x+","+y+","+w+","+h);
            try {
                Canvas cv = lockCanvas();
                if (cv != null) {
                    cv.drawBitmap(mBitmap, mSrcRect, mDstRect, mPaint);
                    unlockCanvasAndPost(cv);
                }
            } catch (Exception e) {
                Log.i(TAG, "updateWindow error:" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Log.i(TAG, "******onSurfaceTextureAvailable:"+width+","+height);
        mSrcRect.set(0, 0, mViewWidth, mViewHeight);
        mDstRect.set(0, 0, width, height);

        if (mPaint == null) {
            mPaint = new Paint();
            mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        }

        if (isGLES2) {
            mHandler.post(new Runnable(){
                public void run(){
                    setSurface(new Surface(getSurfaceTexture()), mViewWidth, mViewHeight);
                }
            });
        }
        sendExposeEvent();
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.i(TAG, "******onSurfaceTextureSizeChanged");
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.i(TAG, "******onSurfaceTextureDestroyed");
        return true;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surface) { }
}

