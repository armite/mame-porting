//============================================================
//
//  Copyright (c) 2014, armite@126.com.
//
//============================================================

#ifndef __JNIHELPER__
#define __JNIHELPER__

#include <stdio.h>
#include <stdlib.h>
#include <jni.h>

#include <android/keycodes.h>
#include <android/input.h>
#include <android/log.h>
#include "osdcore.h"

#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, "GefoMameViewJNI", __VA_ARGS__);

jint JNIHelperInit(JavaVM* vm);
int androidMain(int argc, char *argv[]);

void windowInitBitmap(void* &surfptr, int &stride, int &width, int &height, int &format);
void windowUpdateBitmap();
void windowDestroy();

void addEvent2Buffer(int type, int keycode);

void openAudio(int rate, bool stereo);
void closeAudio();
void playAudio(const void *buffer, int len);

osd_file* openAssetsFile(const char* name);
UINT64 sizeAssetsFile(osd_file* file);
bool readAssetsFile(osd_file* file, void *buffer, UINT64 offset, UINT32 length, UINT32 *actual);
bool closeAssetsFile(osd_file* file);

void startupShowing();

#endif  /* __JNIHELPER__ */
