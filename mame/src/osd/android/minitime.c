// license:BSD-3-Clause
// copyright-holders:Aaron Giles
//============================================================
//
//  minitime.c - Minimal core timing functions
//
//============================================================

#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include "osdepend.h"


//============================================================
//  osd_ticks
//============================================================

osd_ticks_t osd_ticks(void)
{
    struct timeval    tp;
    static osd_ticks_t start_sec = 0;

    gettimeofday(&tp, NULL);
    if (start_sec==0)
        start_sec = tp.tv_sec;
    return (tp.tv_sec - start_sec) * (osd_ticks_t) 1000000 + tp.tv_usec;
}


//============================================================
//  osd_ticks_per_second
//============================================================

osd_ticks_t osd_ticks_per_second(void)
{
    return CLOCKS_PER_SEC;
}


//============================================================
//  osd_sleep
//============================================================

void osd_sleep(osd_ticks_t duration)
{
    UINT32 msec;

    // convert to milliseconds, rounding down
    msec = (UINT32)(duration * 1000 / osd_ticks_per_second());

    // only sleep if at least 2 full milliseconds
    if (msec >= 2)
    {
        // take a couple of msecs off the top for good measure
        msec -= 2;
        usleep(msec*1000);
    }
}
