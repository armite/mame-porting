// license:BSD-3-Clause
// copyright-holders:Aaron Giles
//============================================================
//
//  minimisc.c - Minimal core miscellaneous functions
//
//============================================================

#include "osdcore.h"
#include <sys/mman.h>
#include <signal.h>
#include <stdlib.h>


//============================================================
//  osd_malloc
//============================================================

void *osd_malloc(size_t size)
{
    return malloc(size);
}


//============================================================
//  osd_malloc_array
//============================================================

void *osd_malloc_array(size_t size)
{
    return malloc(size);
}


//============================================================
//  osd_free
//============================================================

void osd_free(void *ptr)
{
    free(ptr);
}


//============================================================
//  osd_alloc_executable
//============================================================

void *osd_alloc_executable(size_t size)
{
    return (void *)mmap(0, size, PROT_EXEC|PROT_READ|PROT_WRITE, MAP_ANON|MAP_SHARED, 0, 0);
}


//============================================================
//  osd_free_executable
//============================================================

void osd_free_executable(void *ptr, size_t size)
{
    munmap(ptr, size);
}


//============================================================
//  osd_break_into_debugger
//============================================================

void osd_break_into_debugger(const char *message)
{
    // there is no standard way to do this, so ignore it
}


//============================================================
//  osd_get_clipboard_text
//============================================================

char *osd_get_clipboard_text(void)
{
    // can't support clipboards generically
    return NULL;
}


//============================================================
//  osd_get_slider_list
//============================================================

const void *osd_get_slider_list()
{
    // nothing to slide in mini OSD
    return NULL;
}
