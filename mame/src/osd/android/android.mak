###########################################################################
#
#   android.mak
#
#   Minimal OSD makefile
#
###########################################################################

#make OSD=android FULLNAME=libmame.so CROSS_BUILD=1 TARGETOS=linux FORCE_DRC_C_BACKEND=1
#-------------------------------------------------
# compile and linking flags
#-------------------------------------------------
NDKDIR=/home/armite/android-ndk-r9
CLANGTOOLCHAIN=$(NDKDIR)/toolchains/llvm-3.5/prebuilt/linux-x86_64/bin/
GCCTOOLCHAIN=$(NDKDIR)/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64
SYSROOT=$(NDKDIR)/platforms/android-19/arch-arm

AR = @$(GCCTOOLCHAIN)/bin/arm-linux-androideabi-ar
CC = @$(CLANGTOOLCHAIN)clang
LD = @$(CLANGTOOLCHAIN)clang++

DEFS += -D_REENTRANT -DDISABLE_MIDI=1 -D_LFS64_LARGEFILE=0 -D_BSD_SETJMP_H -DANDROID -DLSB_FIRST
INCPATH += -I$(NDKDIR)/sources/cxx-stl/llvm-libc++/libcxx/include
INCPATH += -I$(NDKDIR)/sources/cxx-stl/llvm-libc++abi/libcxxabi/include
INCPATH += -I$(NDKDIR)/sources/android/support/include

CCOMFLAGS += -gcc-toolchain $(GCCTOOLCHAIN) -target thumbv7-none-linux-androideabi
CCOMFLAGS += -fpic -ffunction-sections -funwind-tables -fstack-protector -Wno-invalid-command-line-argument
CCOMFLAGS += -Wno-unused-command-line-argument -no-canonical-prefixes -fno-integrated-as
CCOMFLAGS += --sysroot $(SYSROOT) -march=armv7-a -D_NDK_MATH_NO_SOFTFP=1 -mhard-float -mthumb
CCOMFLAGS += -fomit-frame-pointer -fno-strict-aliasing -fsigned-char
CCOMFLAGS += -Wno-sign-compare -Wno-cast-align -Wno-undef -Wno-unused-result -Wno-overflow
CCOMFLAGS += -Wno-narrowing -Wno-deprecated-register -Wno-inline-new-delete
CONLYFLAGS += $(CCOMFLAGS) -Wno-incompatible-pointer-types-discards-qualifiers -Wno-invalid-noreturn
CPPONLYFLAGS += -ffast-math -stdlib=libc++

LIBS += -llog -lOpenSLES -ljnigraphics -landroid -lc -Wl,--no-warn-mismatch -lm_hard -lc++_static
LDFLAGS += -target thumbv7-none-linux-androideabi -gcc-toolchain $(GCCTOOLCHAIN)
LDFLAGS += -L$(NDKDIR)/sources/cxx-stl/llvm-libc++/libs/armeabi-v7a-hard/thumb
LDFLAGS += -no-canonical-prefixes -march=armv7-a --sysroot $(SYSROOT) -shared
LDFLAGS += -Wl,--fix-cortex-a8 -Wl,--no-undefined -Wl,-z,noexecstack -Wl,-z,relro -Wl,-z,now

#-------------------------------------------------
# object and source roots
#-------------------------------------------------

MINISRC = $(SRC)/osd/$(OSD)
MINIOBJ = $(OBJ)/osd/$(OSD)
OBJDIRS += $(MINIOBJ)

#-------------------------------------------------
# OSD core library
#-------------------------------------------------

OSDCOREOBJS = \
	$(MINIOBJ)/minidir.o \
	$(MINIOBJ)/minifile.o \
	$(MINIOBJ)/minimisc.o \
	$(MINIOBJ)/minisync.o \
	$(MINIOBJ)/minitime.o \
	$(MINIOBJ)/miniwork.o \

#-------------------------------------------------
# OSD mini library
#-------------------------------------------------

OSDOBJS = \
	$(MINIOBJ)/minimain.o \
	$(MINIOBJ)/video.o \
	$(MINIOBJ)/audio.o \
	$(MINIOBJ)/opensl_snd.o \
	$(MINIOBJ)/JNIHelper.o \
	$(MINIOBJ)/rendersw.o \

#-------------------------------------------------
# rules for building the libaries
#-------------------------------------------------
$(LIBOCORE): $(OSDCOREOBJS)
$(LIBOSD): $(OSDOBJS)
