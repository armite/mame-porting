// license:BSD-3-Clause
// copyright-holders:armite@126.com
//============================================================
//
//  minimain.c - Main function for mini OSD
//
//============================================================

#include "emu.h"
#include "osdepend.h"
#include "render.h"
#include "clifront.h"
#include "osdmini.h"
#include "JNIHelper.h"


//============================================================
//  main
//============================================================

int androidMain(int argc, char *argv[])
{
    // cli_frontend does the heavy lifting; if we have osd-specific options, we
    // create a derivative of cli_options and add our own
    cli_options options;
    mini_osd_interface osd;
    osd.register_options(options);
    cli_frontend frontend(options, osd);
    astring error;
    options.set_value(OPTION_READCONFIG, "0", OPTION_PRIORITY_HIGH, error);
    options.set_value(OPTION_MEDIAPATH, "assets", OPTION_PRIORITY_HIGH, error);
    options.set_value(OPTION_ARTPATH, "", OPTION_PRIORITY_HIGH, error);
    options.set_value(OPTION_INIPATH, "", OPTION_PRIORITY_HIGH, error);
    options.set_value(OPTION_FONTPATH, "", OPTION_PRIORITY_HIGH, error);
    return frontend.execute(argc, argv);
}


//============================================================
//  constructor
//============================================================

mini_osd_interface::mini_osd_interface()
{
}


//============================================================
//  destructor
//============================================================

mini_osd_interface::~mini_osd_interface()
{
}


//============================================================
//  init
//============================================================

void mini_osd_interface::init(running_machine &machine)
{
    // call our parent
    osd_interface::init(machine);
    osd_interface::init_subsystems();
}

