// license:BSD-3-Clause
// copyright-holders:armite@126.com
//============================================================
//
//  osdmini.h - Core header
//
//============================================================

#include "options.h"
#include "osdepend.h"


//============================================================
//  TYPE DEFINITIONS
//============================================================

class mini_osd_interface : public osd_interface
{
    public:
        // construction/destruction
        mini_osd_interface();
        virtual ~mini_osd_interface();

        // general overridables
        virtual void init(running_machine &machine);

        // video overridables
        virtual bool video_init();
        virtual void update(bool skip_redraw);
        virtual void video_exit();

        // audio overridables
        virtual void sound_register();

        //input overridables
        virtual bool input_init();
        virtual void input_exit();
};


//============================================================
//  GLOBAL VARIABLES
//============================================================


//============================================================
//  FUNCTION PROTOTYPES
//============================================================

