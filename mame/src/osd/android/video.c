//============================================================
//
//  video.c - android video handling
//
//  Copyright (c) 2014, armite@126.com.
//
//============================================================

// MAME headers
#include "emu.h"
#include "rendutil.h"
#include "ui/ui.h"
#include "emuopts.h"
#include "uiinput.h"
#include "rendersw.inc"
#include "rendersw.h"

// MAMEOS headers
#include "osdmini.h"
#include "minisync.h"

#include "JNIHelper.h"
#include <android/bitmap.h>

//============================================================
//  CONSTANTS
//============================================================


//============================================================
//  TYPE DEFINITIONS
//============================================================
struct armite_window_info
{
    render_target   *target;
    osd_work_queue  *workQueue;
    osd_event       *renderedEvent;

    void            *surfptr;
    int             format;
    int             pitch;
    int             width;
    int             height;
};


//============================================================
//  LOCAL VARIABLES
//============================================================
static armite_window_info window = {NULL, NULL, NULL, 0, 0, 0};


//============================================================
//  PROTOTYPES
//============================================================
#define OSDWORK_CALLBACK(name) void *name(void *param, ATTR_UNUSED int threadid)


//============================================================
//  video_init
//============================================================
static OSDWORK_CALLBACK(videoInitCallback)
{
    windowInitBitmap(window.surfptr, window.pitch, window.width, window.height, window.format);
    return NULL;
}

bool mini_osd_interface::video_init()
{
    window.target = machine().render().target_alloc();
    window.workQueue = osd_work_queue_alloc(WORK_QUEUE_FLAG_IO);
    window.renderedEvent = osd_event_alloc(FALSE, TRUE);
    if (!window.target || !window.workQueue || !window.renderedEvent) {
        osd_printf_error("initialize %p %p %p", window.target, window.workQueue, window.renderedEvent);
        return false;
    }

    window.target->compute_minimum_size(window.width, window.height);
    osd_work_item_queue(window.workQueue, &videoInitCallback, NULL, WORK_ITEM_FLAG_AUTO_RELEASE);
    osd_work_queue_wait(window.workQueue, osd_ticks_per_second());
    window.target->set_bounds(window.width, window.height);
    return true;
}


//============================================================
//  video_exit
//============================================================
static OSDWORK_CALLBACK(videoExitCallback)
{
    windowDestroy();
    return NULL;
}

void mini_osd_interface::video_exit()
{
    window.surfptr = NULL;
    osd_work_item* item = osd_work_item_queue(window.workQueue, &videoExitCallback, NULL, 0);
    osd_work_item_release(item);
    osd_work_queue_free(window.workQueue);
    osd_event_free(window.renderedEvent);
    machine().render().target_free(window.target);
}


//============================================================
//  TYPEDEFS
//============================================================

#define MAX_KEYS            256

// generic input information
struct input
{
    astring                 keyboard_name;
    input_device *          keyboard_device;
    astring                 joystick_name;
    input_device *          joystick_device;
    INT32                   state[MAX_KEYS];
};
static input                input;
static osd_lock*            inputLock = NULL;

#define MAX_BUF_EVENTS      (100)
struct event_info
{
    int type;
    int keycode;
};
static event_info       eventBuffer[MAX_BUF_EVENTS];
static int              eventBufferCount;

void addEvent2Buffer(int type, int keycode)
{
    if (!inputLock)
        return;
    osd_lock_acquire(inputLock);
    if (eventBufferCount < MAX_BUF_EVENTS) {
        eventBuffer[eventBufferCount].type = type;
        eventBuffer[eventBufferCount].keycode = keycode;
        eventBufferCount++;
    } else
        osd_printf_warning("Event Buffer Overflow!\n");
    osd_lock_release(inputLock);
}

static inline void controlEvent(running_machine &machine, int keyCode)
{
    switch(keyCode) {
        case AKEYCODE_P:
            machine.pause();
            break;
        case AKEYCODE_R:
            machine.resume();
            break;
        case AKEYCODE_E:
            machine.schedule_exit();
            break;
    }
}

static inline void inputPoll(running_machine &machine)
{
    event_info event;
    event_info loc_eventBuffer[MAX_BUF_EVENTS];
    int loc_eventBufferCount;
    int bufp;

    osd_lock_acquire(inputLock);
    memcpy(loc_eventBuffer, eventBuffer, sizeof(eventBuffer));
    loc_eventBufferCount = eventBufferCount;
    eventBufferCount = 0;
    osd_lock_release(inputLock);
    bufp = 0;

    while (TRUE) {
        if (bufp >= loc_eventBufferCount)
            break;
        event = loc_eventBuffer[bufp++];

        switch(event.type) {
            case AKEY_EVENT_ACTION_DOWN:
                controlEvent(machine, event.keycode);
                input.state[event.keycode] = 0x80;
                break;
            case AKEY_EVENT_ACTION_UP:
                input.state[event.keycode] = 0x00;
                break;
        }
    }
}

//============================================================
//  update
//============================================================
static OSDWORK_CALLBACK(video_update_callback)
{
    if (!window.surfptr)
        return NULL;

    render_primitive_list* list = (render_primitive_list*)param;
    list->acquire_lock();
    if (window.format == ANDROID_BITMAP_FORMAT_RGBA_8888) {
        draw_primitives(*list, (UINT8*)window.surfptr, window.width, window.height, window.pitch);
    } else {
        software_renderer<UINT16, 3, 2, 3, 11, 5, 0>::draw_primitives(*list,
            (UINT8*)window.surfptr, window.width, window.height, window.pitch);
    }
    list->release_lock();

    osd_event_set(window.renderedEvent);
    windowUpdateBitmap();
    return NULL;
}


void mini_osd_interface::update(bool skip_redraw)
{
    // if we're not skipping this redraw, update all windows
    if (!skip_redraw) {
        render_primitive_list* list = &window.target->get_primitives();
        if (osd_event_wait(window.renderedEvent, 0))
            osd_work_item_queue(window.workQueue, &video_update_callback, list, WORK_ITEM_FLAG_AUTO_RELEASE);
    }
    inputPoll(machine());
}


//============================================================
//  generic_get_state
//============================================================

static INT32 generic_get_state(void *device_internal, void *item_internal)
{
    INT32 *itemdata = (INT32 *) item_internal;
    return *itemdata >> 7;
}


//============================================================
//  input_init
//============================================================

bool mini_osd_interface::input_init()
{
    // allocate a lock for input synchronizations
    inputLock = osd_lock_alloc();

    input.keyboard_name = "keyboard";
    input.joystick_name = "joystick";

    // register the keyboards
    input.keyboard_device = machine().input().device_class(DEVICE_CLASS_KEYBOARD).add_device(input.keyboard_name, &input);
    input.keyboard_device->add_item("Up", ITEM_ID_UP, generic_get_state, &input.state[AKEYCODE_W]);
    input.keyboard_device->add_item("Down", ITEM_ID_DOWN, generic_get_state, &input.state[AKEYCODE_S]);
    input.keyboard_device->add_item("Left", ITEM_ID_LEFT, generic_get_state, &input.state[AKEYCODE_A]);
    input.keyboard_device->add_item("Right", ITEM_ID_RIGHT, generic_get_state, &input.state[AKEYCODE_D]);
    input.keyboard_device->add_item("Button 1", ITEM_ID_LCONTROL, generic_get_state, &input.state[AKEYCODE_J]);
    input.keyboard_device->add_item("Button 2", ITEM_ID_LALT, generic_get_state, &input.state[AKEYCODE_K]);
    input.keyboard_device->add_item("Button 3", ITEM_ID_SPACE, generic_get_state, &input.state[AKEYCODE_L]);
    input.keyboard_device->add_item("Start", ITEM_ID_1, generic_get_state, &input.state[AKEYCODE_1]);
    input.keyboard_device->add_item("Coin", ITEM_ID_5, generic_get_state, &input.state[AKEYCODE_5]);

    // register the joysticks
    input.joystick_device = machine().input().device_class(DEVICE_CLASS_JOYSTICK).add_device(input.joystick_name, &input);
    input.joystick_device->add_item("Select", ITEM_ID_SELECT, generic_get_state, &input.state[AKEYCODE_BUTTON_SELECT]);
    input.joystick_device->add_item("Start", ITEM_ID_START, generic_get_state, &input.state[AKEYCODE_BUTTON_START]);

    // now reset all keys
    memset(&input.state, 0, sizeof(input.state));
    return true;
}

//============================================================
//  input_exit
//============================================================

void mini_osd_interface::input_exit()
{
    // free the lock
    osd_lock_free(inputLock);
    inputLock = NULL;
}

