//============================================================
//
//  JNIHelper.c - android jni helper
//
//  Copyright (c) 2014, armite@126.com.
//
//============================================================

#include "JNIHelper.h"

#include <android/bitmap.h>
#include <sys/system_properties.h>
#include <android/asset_manager_jni.h>

struct AndroidJni {
    JavaVM* vm;
    jobject oMameView;
    jobject oAssetManager;

    JNIEnv      *env;
    jmethodID   getBitmap;
    jmethodID   updateBitmap;

    jmethodID   audioOpen;
    jmethodID   audioClose;
    jmethodID   audioPlay;

    jmethodID   startupShowing;

    AAsset*     assetFile;
} androidJni;

void windowInitBitmap(void* &surfptr, int &pitch, int &width, int &height, int &format)
{
    surfptr = NULL;
    pitch = 0;
    androidJni.vm->AttachCurrentThread(&androidJni.env, NULL);
    jobject bitmap = androidJni.env->CallObjectMethod(androidJni.oMameView, androidJni.getBitmap, width, height);

    AndroidBitmapInfo   info;
    void*               pixels;
    int                 ret;
    if ((ret = AndroidBitmap_getInfo(androidJni.env, bitmap, &info)) < 0) {
        LOGI("AndroidBitmap_getInfo() failed ! error=%d", ret);
        return;
    }
    if ((ret = AndroidBitmap_lockPixels(androidJni.env, bitmap, &pixels)) < 0) {
        LOGI("AndroidBitmap_lockPixels() failed ! error=%d", ret);
        return;
    }
    AndroidBitmap_unlockPixels(androidJni.env, bitmap);

    surfptr = pixels;
    format = info.format;
    if (format == ANDROID_BITMAP_FORMAT_RGBA_8888)
        pitch = info.stride / 4;
    else
        pitch = info.stride / 2;
    width = info.width;
    height = info.height;
    LOGI("bitmap init %p %d %d %d", surfptr, pitch, width, height);
}

void windowUpdateBitmap()
{
    androidJni.env->CallVoidMethod(androidJni.oMameView, androidJni.updateBitmap);
}

void windowDestroy()
{
    androidJni.vm->DetachCurrentThread();
    LOGI("window destroy");
}

static void MameView_sendKeyEvent(JNIEnv* env, jobject obj, jint type, jint keycode)
{
    addEvent2Buffer(type, keycode);
}

static void MameView_runGame(JNIEnv* env, jobject obj, jobject am, jstring path, jstring name)
{
    androidJni.oMameView = env->NewGlobalRef(obj);
    androidJni.oAssetManager = env->NewGlobalRef(am);
    androidJni.assetFile = NULL;

    int argc = 0;
    static const char *argv[8];
    argv[argc++] = "mame4x";
    if (path) {
        jboolean jret;
        const char *jpath = env->GetStringUTFChars(path, &jret);
        argv[argc++] = "-rompath";
        argv[argc++] = strdup(jpath);
        if(jret)
            env->ReleaseStringUTFChars(path, jpath);
    }
    if (name) {
        jboolean jret;
        const char *jname = env->GetStringUTFChars(name, &jret);
        argv[argc++] = strdup(jname);
        if(jret)
            env->ReleaseStringUTFChars(name, jname);
    }
    //argv[argc++] = "-v";
    //argv[argc++] = "-sound";
    //argv[argc++] = "none";
    androidMain(argc, (char **)argv);

    env->DeleteGlobalRef(androidJni.oAssetManager);
    env->DeleteGlobalRef(androidJni.oMameView);
}

static int buffersize = 3840;
void openAudio(int rate, bool stereo)
{
    JNIEnv *env = NULL;
    androidJni.vm->GetEnv((void**)(&env), JNI_VERSION_1_6);
    if (!env || !androidJni.audioOpen)
        return;
    buffersize = env->CallIntMethod(androidJni.oMameView, androidJni.audioOpen, rate, stereo);
}

void closeAudio()
{
    JNIEnv *env = NULL;
    androidJni.vm->GetEnv((void**)(&env), JNI_VERSION_1_6);
    if (!env || !androidJni.audioClose)
        return;
    env->CallVoidMethod(androidJni.oMameView, androidJni.audioClose);
}

static jbyteArray jbaAudioBuffer = NULL;
void playAudio(const void *buffer, int len)
{
    JNIEnv *env = NULL;
    androidJni.vm->GetEnv((void**)(&env), JNI_VERSION_1_6);
    if (!env || !androidJni.audioPlay)
        return;
    if(jbaAudioBuffer == NULL) {
        jbaAudioBuffer=env->NewByteArray(buffersize);
        jobject tmp = jbaAudioBuffer;
        jbaAudioBuffer=(jbyteArray)env->NewGlobalRef(jbaAudioBuffer);
        env->DeleteLocalRef(tmp);
    }
    if (len > buffersize)
        len = buffersize;
    env->SetByteArrayRegion(jbaAudioBuffer, 0, len, (jbyte *)buffer);
    env->CallVoidMethod(androidJni.oMameView, androidJni.audioPlay, jbaAudioBuffer, len);
}

osd_file* openAssetsFile(const char* name)
{
    if (androidJni.assetFile != NULL) {
        LOGI("there is only a asset file, open asset file error:%s", name);
        return NULL;
    }

    if (strchr(name, '/') != NULL)
        return NULL;

    JNIEnv *env = NULL;
    androidJni.vm->GetEnv((void**)(&env), JNI_VERSION_1_6);
    if (!env)
        return NULL;

    AAssetManager* assetManager = AAssetManager_fromJava(env, androidJni.oAssetManager);
    androidJni.assetFile = AAssetManager_open(assetManager, name, AASSET_MODE_RANDOM);
    return (osd_file*)androidJni.assetFile;
}

UINT64 sizeAssetsFile(osd_file* file)
{
    if (androidJni.assetFile == (AAsset*)file)
        return AAsset_getLength64(androidJni.assetFile);
    return 0;
}

bool readAssetsFile(osd_file* file, void *buffer, UINT64 offset, UINT32 length, UINT32 *actual)
{
    if (androidJni.assetFile != (AAsset*)file)
        return false;
    AAsset_seek64(androidJni.assetFile, offset, SEEK_SET);
    int count = 0;
    count = AAsset_read(androidJni.assetFile, buffer, length);
    if (count < 0) {
        LOGI("read asset file error:%d", count);
        count = 0;
    }
    if (actual != NULL)
        *actual = count;
    return true;
}

bool closeAssetsFile(osd_file* file)
{
    if (androidJni.assetFile == (AAsset*)file) {
        AAsset_close(androidJni.assetFile);
        androidJni.assetFile = NULL;
        return true;
    }
    return false;
}

void startupShowing()
{
    JNIEnv *env = NULL;
    androidJni.vm->GetEnv((void**)(&env), JNI_VERSION_1_6);
    if (!env || !androidJni.startupShowing) {
        LOGI("startupShowing error:%p,%p", env, androidJni.startupShowing);
        return;
    }
    env->CallVoidMethod(androidJni.oMameView, androidJni.startupShowing);
}

static const JNINativeMethod gMethods[] = {
    {"sendKeyEvent", "(II)V", (void *)MameView_sendKeyEvent},
    {"runGame", "(Landroid/content/res/AssetManager;Ljava/lang/String;Ljava/lang/String;)V",(void *)MameView_runGame},
};

jint JNIHelperInit(JavaVM* vm)
{
    const char *className = "com/armite/WebKit/MameView";
    char propName[PROP_VALUE_MAX] = {'\0'};
    __system_property_get("armite.mame.class", propName);
    if (strlen(propName) > 0) {
        LOGI("system_property_get class name:%s", propName);
        className = propName;
    }

    androidJni.vm = vm;
    JNIEnv *env;
    if(vm->GetEnv((void**)(&env), JNI_VERSION_1_6) != JNI_OK) {
        LOGI("Failed to get the environment using GetEnv()");
        return -1;
    }

    jclass cMameView = NULL;
    cMameView = env->FindClass(className);
    if(cMameView == NULL) {
        LOGI("Failed to find class %s", className);
        return -1;
    }

    if(env->RegisterNatives(cMameView, gMethods, sizeof(gMethods) / sizeof(gMethods[0])) != JNI_OK) {
        LOGI("Failed registering methods for MameView");
        return -1;
    }

    androidJni.getBitmap = env->GetMethodID(cMameView, "getBitmap","(II)Landroid/graphics/Bitmap;");
    if(androidJni.getBitmap == NULL) {
        LOGI("Failed to find method getBitmap");
        return -1;
    }

    androidJni.updateBitmap = env->GetMethodID(cMameView, "updateBitmap","()V");
    if(androidJni.updateBitmap == NULL) {
        LOGI("Failed to find method updateBitmap");
        return -1;
    }

    androidJni.audioOpen = env->GetMethodID(cMameView, "audioOpen","(IZ)I");
    androidJni.audioClose = env->GetMethodID(cMameView, "audioClose","()V");
    androidJni.audioPlay = env->GetMethodID(cMameView, "audioPlay","([BI)V");

    androidJni.startupShowing = env->GetMethodID(cMameView, "startupShowing","()V");
    return JNI_VERSION_1_6;
}

