//============================================================
//
//  audio.c - android implementation of MAME sound routines
//
//  Copyright (c) 2014, armite@126.com.
//
//============================================================

#include "opensl_snd.h"
#include "JNIHelper.h"

// MAME headers
#include "emu.h"
#include "emuopts.h"

#include "osdmini.h"
#include "audio.h"

//============================================================
//  LOCAL VARIABLES
//============================================================
static const bool useOpensl = true;
static OPENSL_SND *p = NULL;

//============================================================
//  PROTOTYPES
//============================================================

static const osd_sound_type OSD_SOUND_ARMITE = &osd_sound_creator<sound_armite>;
void mini_osd_interface::sound_register()
{
    sound_options_add("auto", OSD_SOUND_ARMITE);
}


//-------------------------------------------------
//  sound_armite - constructor
//-------------------------------------------------

sound_armite::sound_armite(const osd_interface &osd)
    : osd_sound_interface(osd)
{
    // skip if sound disabled
    if (osd.machine().sample_rate() != 0) {
        osd_printf_verbose("openAudio:%d\n",osd.machine().sample_rate());
        if (useOpensl) {
            opensl_close(p);
            p = opensl_open(osd.machine().sample_rate(), 2, 512);
        } else {
            closeAudio();
            openAudio(osd.machine().sample_rate(), true);
        }
    }
}


//============================================================
//  sound_armite - destructor
//============================================================

sound_armite::~sound_armite()
{
    // if nothing to do, don't do it
    if (m_osd.machine().sample_rate() == 0)
        return;
    if (useOpensl) {
        opensl_close(p);
        p = NULL;
    } else
        closeAudio();
}


//============================================================
//  update_audio_stream
//============================================================

void sound_armite::update_audio_stream(const INT16 *buffer, int samples_this_frame)
{
    // if nothing to do, don't do it
    if (m_osd.machine().sample_rate() != 0) {
        //osd_printf_verbose("update_audio_stream:%d\n", samples_this_frame);
        if (useOpensl)
            opensl_write(p, buffer, samples_this_frame * 2);
        else
            playAudio(buffer, samples_this_frame * sizeof(INT16) * 2);
    }
}


//============================================================
//  set_mastervolume
//============================================================

void sound_armite::set_mastervolume(int _attenuation)
{
}

