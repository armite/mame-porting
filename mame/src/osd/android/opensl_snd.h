//============================================================
//
//  opensl_snd.c - Implementation of osd stuff
//
//  Copyright (c) 2014, armite@126.com.
//
//============================================================

#ifndef OPENSL_SOUND
#define OPENSL_SOUND

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <stdlib.h>
#include <unistd.h>
#include "osdcomm.h"

typedef struct opensl_snd {
    SLObjectItf engineObject;
    SLEngineItf engineEngine;
    SLObjectItf outputMixObject;
    SLObjectItf bqPlayerObject;
    SLPlayItf bqPlayerPlay;
    SLAndroidSimpleBufferQueueItf bqPlayerBufferQueue;
    INT16 *outputBuffer;
    INT16 *playBuffer[2];
    int outBufSamples;
    int outchannels;
    int   sr;
    INT16 currPlayBuffer;
} OPENSL_SND;

OPENSL_SND* opensl_open(int sr, int outchannels, int bufferframes);
void opensl_close(OPENSL_SND *p);
int opensl_write(OPENSL_SND *p, const INT16 *buffer,int size);

#endif
