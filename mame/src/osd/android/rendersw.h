//============================================================
//
//  Copyright (c) 2014, armite@126.com.
//
//============================================================

#ifndef __RENDERSW__
#define __RENDERSW__

#include <stdio.h>
#include <stdlib.h>
#include "emu.h"

void draw_primitives(const render_primitive_list &primlist, void *dstdata, UINT32 width, UINT32 height, UINT32 pitch);

#endif  /* __RENDERSW__ */
