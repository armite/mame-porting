//============================================================
//
//  video.c - SDL video handling
//
//  Copyright (c) 2014, armite@126.com.
//
//============================================================

#include <SDL2/SDL.h>
#include <SDL2/SDL_version.h>
#include <SDL2/SDL_syswm.h>

// MAME headers
#include "emu.h"
#include "rendutil.h"
#include "ui/ui.h"
#include "emuopts.h"
#include "uiinput.h"
#include "rendersw.inc"

// MAMEOS headers
#include "osdmini.h"

//============================================================
//  CONSTANTS
//============================================================


//============================================================
//  TYPE DEFINITIONS
//============================================================
struct armite_window_info
{
    render_target       *target;
    osd_work_queue      *work_queue;

    SDL_Window          *window;
    SDL_Renderer        *renderer;
    SDL_Texture         *texture;
    int                 width;
    int                 height;
};


//============================================================
//  LOCAL VARIABLES
//============================================================
static armite_window_info window = {NULL, NULL, NULL, NULL, NULL, 0, 0};


//============================================================
//  PROTOTYPES
//============================================================
#define OSDWORK_CALLBACK(name) void *name(void *param, ATTR_UNUSED int threadid)


//============================================================
//  video_init
//============================================================
static OSDWORK_CALLBACK(video_init_callback)
{
    window.width = 640;
    window.height = 480;

    window.window = SDL_CreateWindow("armite-mame", 0, 0, window.width, window.height, SDL_WINDOW_RESIZABLE);
    SDL_ShowWindow(window.window);
    SDL_RaiseWindow(window.window);
    window.renderer = SDL_CreateRenderer(window.window, -1, SDL_RENDERER_PRESENTVSYNC);
    window.texture=SDL_CreateTexture(window.renderer,SDL_PIXELFORMAT_ARGB8888,SDL_TEXTUREACCESS_STREAMING,window.width,window.height);
    return NULL;
}

bool mini_osd_interface::video_init()
{
    window.target = machine().render().target_alloc();
    window.work_queue = osd_work_queue_alloc(WORK_QUEUE_FLAG_IO);
    if (!window.target || !window.work_queue) {
        osd_printf_error("Could not initialize %p %p\n", window.target, window.work_queue);
        return false;
    }

    if (SDL_InitSubSystem(SDL_INIT_TIMER| SDL_INIT_VIDEO| SDL_INIT_JOYSTICK| SDL_INIT_NOPARACHUTE)) {
        osd_printf_error("Could not initialize SDL %s\n", SDL_GetError());
        return false;
    }

    osd_work_item_queue(window.work_queue, &video_init_callback, NULL, WORK_ITEM_FLAG_AUTO_RELEASE);
    osd_work_queue_wait(window.work_queue, osd_ticks_per_second());

    int tempwidth, tempheight;
    window.target->compute_minimum_size(tempwidth, tempheight);
    if (window.width > tempwidth)
        tempwidth = window.width;
    if (window.height > tempheight)
        tempheight = window.height;
    window.target->set_bounds(tempwidth, tempheight);
    return true;
}


//============================================================
//  video_exit
//============================================================
static OSDWORK_CALLBACK(video_exit_callback)
{
    SDL_DestroyTexture(window.texture);
    SDL_DestroyWindow(window.window);
    return NULL;
}

void mini_osd_interface::video_exit()
{
    osd_work_item_queue(window.work_queue, &video_exit_callback, NULL, WORK_ITEM_FLAG_AUTO_RELEASE);
    osd_work_queue_free(window.work_queue);
    machine().render().target_free(window.target);
}


//============================================================
//  TYPEDEFS
//============================================================

// generic keyboard information
struct keyboard_info
{
    astring                 name;
    input_device *          device;
    INT32                   state[0x3ff];
};
static keyboard_info        keyboard;
static osd_lock *           input_lock;

#define MAX_BUF_EVENTS      (100)
static SDL_Event            event_buf[MAX_BUF_EVENTS];
static int                  event_buf_count;

static void input_process_events_buf()
{
    SDL_Event event;
    osd_lock_acquire(input_lock);
    SDL_PumpEvents();
    while(SDL_PollEvent(&event))
    {
        if (event_buf_count < MAX_BUF_EVENTS)
            event_buf[event_buf_count++] = event;
        else
            osd_printf_warning("Event Buffer Overflow!\n");
    }
    osd_lock_release(input_lock);
}

static void input_poll(running_machine &machine)
{
    SDL_Event event;
    SDL_Event           loc_event_buf[MAX_BUF_EVENTS];
    int                 loc_event_buf_count;
    int bufp;

    osd_lock_acquire(input_lock);
    memcpy(loc_event_buf, event_buf, sizeof(event_buf));
    loc_event_buf_count = event_buf_count;
    event_buf_count = 0;
    osd_lock_release(input_lock);
    bufp = 0;

    while (TRUE)
    {
        if (bufp >= loc_event_buf_count)
            break;
        event = loc_event_buf[bufp++];

        switch(event.type) {
            case SDL_KEYDOWN:
                //printf("Key down %d %d)\n", event.key.repeat, event.key.keysym.scancode);
                keyboard.state[event.key.keysym.scancode] = 0x80;
                break;
            case SDL_KEYUP:
                keyboard.state[event.key.keysym.scancode] = 0x00;
                break;
            case SDL_JOYAXISMOTION:
                break;
            case SDL_JOYHATMOTION:
                break;
            case SDL_JOYBUTTONDOWN:
            case SDL_JOYBUTTONUP:
                break;
            case SDL_JOYBALLMOTION:
                break;
            case SDL_WINDOWEVENT:
                {
                    switch (event.window.event)
                    {
                        case SDL_WINDOWEVENT_CLOSE:
                            machine.schedule_exit();
                            break;
                    }
                    break;
                }
        }
    }
}

//============================================================
//  update
//============================================================
static OSDWORK_CALLBACK(video_update_callback)
{
    input_process_events_buf();

    UINT8 *surfptr;
    INT32 pitch;
    SDL_LockTexture(window.texture, NULL, (void **) &surfptr, &pitch);

    render_primitive_list* list = (render_primitive_list*)param;
    list->acquire_lock();
    software_renderer<UINT32, 0,0,0, 16,8,0>::draw_primitives(*list, surfptr, window.width, window.height, pitch / 4);
    list->release_lock();

    SDL_UnlockTexture(window.texture);
    SDL_RenderCopy(window.renderer,window.texture, NULL, NULL);
    SDL_RenderPresent(window.renderer);
    return NULL;
}

void mini_osd_interface::update(bool skip_redraw)
{
    // if we're not skipping this redraw, update all windows
    if (!skip_redraw)
    {
        render_primitive_list* list = &window.target->get_primitives();
        osd_work_item_queue(window.work_queue, &video_update_callback, list, WORK_ITEM_FLAG_AUTO_RELEASE);
    }
    input_poll(machine());
}


//============================================================
//  generic_get_state
//============================================================

static INT32 generic_get_state(void *device_internal, void *item_internal)
{
    INT32 *itemdata = (INT32 *) item_internal;

    // return the current state
    return *itemdata >> 7;
}


//============================================================
//  input_init
//============================================================

bool mini_osd_interface::input_init()
{
    // allocate a lock for input synchronizations
    input_lock = osd_lock_alloc();

    keyboard.name = "keyboard";

    // register the keyboards
    keyboard.device = machine().input().device_class(DEVICE_CLASS_KEYBOARD).add_device(keyboard.name, &keyboard);
    keyboard.device->add_item("UI Select", ITEM_ID_ENTER, generic_get_state, &keyboard.state[SDL_SCANCODE_RETURN]);
    keyboard.device->add_item("Esc", ITEM_ID_ESC, generic_get_state, &keyboard.state[SDL_SCANCODE_ESCAPE]);
    keyboard.device->add_item("Up", ITEM_ID_UP, generic_get_state, &keyboard.state[SDL_SCANCODE_W]);
    keyboard.device->add_item("Down", ITEM_ID_DOWN, generic_get_state, &keyboard.state[SDL_SCANCODE_S]);
    keyboard.device->add_item("Left", ITEM_ID_LEFT, generic_get_state, &keyboard.state[SDL_SCANCODE_A]);
    keyboard.device->add_item("Right", ITEM_ID_RIGHT, generic_get_state, &keyboard.state[SDL_SCANCODE_D]);
    keyboard.device->add_item("Button 1", ITEM_ID_LCONTROL, generic_get_state, &keyboard.state[SDL_SCANCODE_J]);
    keyboard.device->add_item("Button 2", ITEM_ID_LALT, generic_get_state, &keyboard.state[SDL_SCANCODE_K]);
    keyboard.device->add_item("Button 3", ITEM_ID_SPACE, generic_get_state, &keyboard.state[SDL_SCANCODE_L]);
    keyboard.device->add_item("Start", ITEM_ID_1, generic_get_state, &keyboard.state[SDL_SCANCODE_1]);
    keyboard.device->add_item("Select", ITEM_ID_5, generic_get_state, &keyboard.state[SDL_SCANCODE_5]);

    // now reset all devices
    memset(&keyboard.state, 0, sizeof(keyboard.state));
    return true;
}

//============================================================
//  input_exit
//============================================================

void mini_osd_interface::input_exit()
{
    // free the lock
    osd_lock_free(input_lock);
}

