//============================================================
//
//  audio.h - SDL implementation of MAME sound routines
//
//  Copyright (c) 2014, armite@126.com.
//
//============================================================

#ifndef __SOUND_ARMITE_H__
#define __SOUND_ARMITE_H__

#include "osdepend.h"

class sound_armite : public osd_sound_interface
{
    public:
        // construction/destruction
        sound_armite(const osd_interface &osd);
        virtual ~sound_armite();

        virtual void update_audio_stream(const INT16 *buffer, int samples_this_frame);
        virtual void set_mastervolume(int attenuation);
};

#endif  /* __SOUND_ARMITE_H__ */
