// license:BSD-3-Clause
// copyright-holders:armite@126.com
//============================================================
//
//  minimain.c - Main function for mini OSD
//
//============================================================

#include "emu.h"
#include "osdepend.h"
#include "render.h"
#include "clifront.h"
#include "osdmini.h"


//============================================================
//  main
//============================================================

int main(int argc, char *argv[])
{
    setvbuf(stdout, (char *) NULL, _IONBF, 0);
    setvbuf(stderr, (char *) NULL, _IONBF, 0);

    // cli_frontend does the heavy lifting; if we have osd-specific options, we
    // create a derivative of cli_options and add our own
    cli_options options;
    mini_osd_interface osd;
    osd.register_options(options);
    cli_frontend frontend(options, osd);
    astring error;
    options.set_value(OPTION_MEDIAPATH, "/home/armite/game/mame/roms", OPTION_PRIORITY_HIGH, error);
    return frontend.execute(argc, argv);
}


//============================================================
//  constructor
//============================================================

mini_osd_interface::mini_osd_interface()
{
}


//============================================================
//  destructor
//============================================================

mini_osd_interface::~mini_osd_interface()
{
}


//============================================================
//  init
//============================================================

void mini_osd_interface::init(running_machine &machine)
{
    // call our parent
    osd_interface::init(machine);
    osd_interface::init_subsystems();
}

