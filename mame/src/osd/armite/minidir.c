// license:BSD-3-Clause
// copyright-holders:Aaron Giles
//============================================================
//
//  minidir.c - Minimal core directory access functions
//
//============================================================

#ifndef _LARGEFILE64_SOURCE
#define _LARGEFILE64_SOURCE
#endif

#define __USE_LARGEFILE64

#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

#include "osdcore.h"

#define PATHSEPCH '/'
#define INVPATHSEPCH '\\'

typedef struct dirent64 sdl_dirent;
typedef struct stat64 sdl_stat;
#define sdl_readdir readdir64
#define sdl_stat_fn stat64

struct osd_directory
{
    osd_directory_entry ent;
    sdl_dirent *data;
    DIR *fd;
    char *path;
};

static char *build_full_path(const char *path, const char *file)
{
    char *ret = (char *) osd_malloc_array(strlen(path)+strlen(file)+2);
    char *p = ret;

    strcpy(p, path);
    p += strlen(path);
    *p++ = PATHSEPCH;
    strcpy(p, file);
    return ret;
}

static UINT64 osd_get_file_size(const char *file)
{
    sdl_stat st;
    if(sdl_stat_fn(file, &st))
        return 0;
    return st.st_size;
}

static osd_dir_entry_type get_attributes_enttype(int attributes, char *path)
{
    switch ( attributes )
    {
        case DT_DIR:
            return ENTTYPE_DIR;

        case DT_REG:
            return ENTTYPE_FILE;

        case DT_LNK:
            {
                struct stat s;

                if ( stat(path, &s) != 0 )
                    return ENTTYPE_OTHER;
                else
                    return S_ISDIR(s.st_mode) ? ENTTYPE_DIR : ENTTYPE_FILE;
            }

        default:
            return ENTTYPE_OTHER;
    }
}


//============================================================
//  osd_opendir
//============================================================

osd_directory *osd_opendir(const char *dirname)
{
    osd_directory *dir = NULL;
    char *tmpstr, *envstr;
    int i, j;

    dir = (osd_directory *) osd_malloc(sizeof(osd_directory));
    if (dir)
    {
        memset(dir, 0, sizeof(osd_directory));
        dir->fd = NULL;
    }

    tmpstr = (char *) osd_malloc_array(strlen(dirname)+1);
    strcpy(tmpstr, dirname);

    if (tmpstr[0] == '$')
    {
        char *envval;
        envstr = (char *) osd_malloc_array(strlen(tmpstr)+1);

        strcpy(envstr, tmpstr);

        i = 0;
        while (envstr[i] != PATHSEPCH && envstr[i] != INVPATHSEPCH && envstr[i] != 0 && envstr[i] != '.')
        {
            i++;
        }

        envstr[i] = '\0';

        envval = getenv(&envstr[1]);
        if (envval != NULL)
        {
            j = strlen(envval) + strlen(tmpstr) + 1;
            osd_free(tmpstr);
            tmpstr = (char *) osd_malloc_array(j);

            // start with the value of $HOME
            strcpy(tmpstr, envval);
            // replace the null with a path separator again
            envstr[i] = PATHSEPCH;
            // append it
            strcat(tmpstr, &envstr[i]);
        }
        else
            fprintf(stderr, "Warning: osd_opendir environment variable %s not found.\n", envstr);
        osd_free(envstr);
    }

    dir->fd = opendir(tmpstr);
    dir->path = tmpstr;

    if (dir && (dir->fd == NULL))
    {
        osd_free(dir->path);
        osd_free(dir);
        dir = NULL;
    }

    return dir;

}


//============================================================
//  osd_readdir
//============================================================

const osd_directory_entry *osd_readdir(osd_directory *dir)
{
    char *temp;
    dir->data = sdl_readdir(dir->fd);

    if (dir->data == NULL)
        return NULL;

    dir->ent.name = dir->data->d_name;
    temp = build_full_path(dir->path, dir->data->d_name);
    dir->ent.type = get_attributes_enttype(dir->data->d_type, temp);
    dir->ent.size = osd_get_file_size(temp);
    osd_free(temp);
    return &dir->ent;
}


//============================================================
//  osd_closedir
//============================================================

void osd_closedir(osd_directory *dir)
{
    if (dir->fd != NULL)
        closedir(dir->fd);
    osd_free(dir->path);
    osd_free(dir);
}


//============================================================
//  osd_is_absolute_path
//============================================================

int osd_is_absolute_path(const char *path)
{
    // assume no for everything
    return FALSE;
}
