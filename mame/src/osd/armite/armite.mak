###########################################################################
#
#   armite.mak
#
#   Minimal OSD makefile
#
###########################################################################

#make OSD=armite
#make OSD=armite DEBUG=1 SYMBOLS=1
#make OSD=armite PROFILE=1
#-------------------------------------------------
# compile and linking flags
#-------------------------------------------------
DEFS += -D_REENTRANT -DDISABLE_MIDI=1 -D_LFS64_LARGEFILE=0
#CCOMFLAGS += -Wno-inline-new-delete -Wno-unused-result -Wno-deprecated-register -Wno-c++11-narrowing
#CCOMFLAGS += -m64 -Wno-dynamic-class-memaccess -Wno-implicit-function-declaration
#CONLYFLAGS += -Wno-invalid-noreturn
CCOMFLAGS += -Wno-unused-result -m64 -Wno-maybe-uninitialized
CONLYFLAGS += -Wno-implicit-function-declaration -Wno-error
LIBS += -lpthread -L/usr/lib/x86_64-linux-gnu -lSDL2
LDFLAGS += -m64

#-------------------------------------------------
# object and source roots
#-------------------------------------------------

MINISRC = $(SRC)/osd/$(OSD)
MINIOBJ = $(OBJ)/osd/$(OSD)
OBJDIRS += $(MINIOBJ)

#-------------------------------------------------
# OSD core library
#-------------------------------------------------

OSDCOREOBJS = \
	$(MINIOBJ)/minidir.o \
	$(MINIOBJ)/minifile.o \
	$(MINIOBJ)/minimisc.o \
	$(MINIOBJ)/minisync.o \
	$(MINIOBJ)/minitime.o \
	$(MINIOBJ)/miniwork.o \

#-------------------------------------------------
# OSD mini library
#-------------------------------------------------

OSDOBJS = \
	$(MINIOBJ)/minimain.o \
	$(MINIOBJ)/video.o \
	$(MINIOBJ)/audio.o \

#-------------------------------------------------
# rules for building the libaries
#-------------------------------------------------
$(LIBOCORE): $(OSDCOREOBJS)
$(LIBOSD): $(OSDOBJS)
